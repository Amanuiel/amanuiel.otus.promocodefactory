﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<HttpStatusCode> CreateAsync(T entity)
        {
            if (entity.Id == Guid.Empty)
            {
                entity.Id = Guid.NewGuid();
            }

            if (Data.Any(p => p.Id == entity.Id))
            {
                return Task.FromResult(HttpStatusCode.Conflict);
            }

            Data = Data.Concat(new List<T> {entity});
            
            return Task.FromResult(HttpStatusCode.OK);
        }

        public Task<HttpStatusCode> UpdateAsync(T entity)
        {
            if (entity.Id == Guid.Empty || Data.All(p => p.Id != entity.Id))
            {
                return Task.FromResult(HttpStatusCode.NotFound);
            }

            var currentEntity = Data.First(p => p.Id == entity.Id);
            currentEntity.ChangeEntity(entity);

            return Task.FromResult(HttpStatusCode.OK);
        }

        public Task<HttpStatusCode> DeleteAsync(Guid id)
        {
            if (Data.All(p => p.Id != id))
            {
                return Task.FromResult(HttpStatusCode.NotFound);
            }

            Data = Data.Where(p => p.Id != id);

            return Task.FromResult(HttpStatusCode.OK);
        }
    }
}