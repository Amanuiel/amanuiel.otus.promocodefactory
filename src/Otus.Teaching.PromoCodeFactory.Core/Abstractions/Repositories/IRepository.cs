﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<HttpStatusCode> CreateAsync(T entity);

        Task<HttpStatusCode> UpdateAsync(T entity);

        Task<HttpStatusCode> DeleteAsync(Guid id);
    }
}