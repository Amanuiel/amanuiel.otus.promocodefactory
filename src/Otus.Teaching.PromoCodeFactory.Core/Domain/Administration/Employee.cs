﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public List<Role> Roles { get; set; }

        public int AppliedPromoCodesCount { get; set; }

        public override void ChangeEntity(BaseEntity entity)
        {
            base.ChangeEntity(entity);

            if (entity is Employee employee)
            {
                FirstName = employee.FirstName;
                LastName = employee.LastName;
                Email = employee.Email;
                Roles = employee.Roles;
                AppliedPromoCodesCount = employee.AppliedPromoCodesCount;
            }
        }
    }
}