﻿using System;
using System.Security.Cryptography;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public override void ChangeEntity(BaseEntity entity)
        {
            base.ChangeEntity(entity);

            if (entity is Role role)
            {
                Name = role.Name;
                Description = role.Description;
            }
        }
    }
}